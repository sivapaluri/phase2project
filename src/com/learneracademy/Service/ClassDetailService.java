package com.learneracademy.Service;

import java.util.List;

import com.learneracademy.model.ClassDetail;

public interface ClassDetailService {
	public ClassDetail createClassDetail(ClassDetail classdetail);

	public List<ClassDetail> getClassDetails();

	public ClassDetail updateClassDetail(ClassDetail classdetail);

	public ClassDetail getClassDetailById(int classId);

	public void removeClassDetail(int classId);

}
