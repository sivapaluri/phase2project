package com.learneracademy.controller;


import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.learneracademy.Service.StudentDetailService;
import com.learneracademy.model.StudentDetail;
import com.learneracademy.serviceImplementation.StudentDetailServiceImpl;

@Path("/studentdetail")
public class StudentDetailController {
	
	StudentDetailService studentdetailservice = new StudentDetailServiceImpl();
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public StudentDetail createStudent(StudentDetail studentdetail) {
		return studentdetailservice.createStudentdetail(studentdetail);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<StudentDetail> getStudentDetails() {
		return studentdetailservice.getStudentDetails();
	}
	
	@PATCH
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public StudentDetail updateStudentDetail(StudentDetail studentdetail) {
		return studentdetailservice.updateStudentDetail(studentdetail);
	}
	
	@Path("/{studentRoll}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public StudentDetail getStudentDetailById(@PathParam("studentRoll")int studentRoll) {
		return studentdetailservice.getStudentDetailById(studentRoll);
	}
	
	@Path("/{studentRoll}")
	@DELETE
	public void removeStudentDetail(@PathParam("studentRoll")int studentRoll) {
		studentdetailservice.removeStudentDetail(studentRoll);
	}
	

}
