package com.learneracademy.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.learneracademy.Service.ClassDetailService;
import com.learneracademy.model.ClassDetail;
import com.learneracademy.serviceImplementation.ClassDetailServiceImpl;

@Path("/classdetail")
public class ClassDetailController {

	ClassDetailService classdetailservice = new ClassDetailServiceImpl();

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ClassDetail createClassDetail(ClassDetail classdetail) {
		return classdetailservice.createClassDetail(classdetail);

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<ClassDetail> getClassDetails() {
		return classdetailservice.getClassDetails();

	}

	@PATCH
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ClassDetail updateClassDetail(ClassDetail classdetail) {
		return classdetailservice.updateClassDetail(classdetail);
	}

	
	@Path("/{classId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ClassDetail getClassDetailById(@PathParam("{classId}") int id) {
		return classdetailservice.getClassDetailById(id);
	}

	@Path("/{classId}")
	@DELETE
	public void removeClassDetail(@PathParam("classId") int id) {
		classdetailservice.removeClassDetail(id);
	}
}
