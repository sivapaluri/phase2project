package com.learneracademy.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/")
public class AcademyController {
	
	@GET
	public String sayhelloGet() {

		return "Welcome to Learners Academy";

	}

}
