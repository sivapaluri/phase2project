package com.learneracademy.DataAccessObjectImplementation;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.learneracademy.DataAccessObject.ClassDetailDAO;
import com.learneracademy.model.ClassDetail;
import com.learneracademy.model.TeacherDetail;

public class ClassDetailDAOImpl implements ClassDetailDAO {

	Configuration configuration = new Configuration().configure();
	
	TeacherDetailDAOImpl tdi = new TeacherDetailDAOImpl();

	StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
			.applySettings(configuration.getProperties());

	SessionFactory factory = configuration.buildSessionFactory(builder.build());

	Session session = factory.openSession();

	@Override
	public ClassDetail createClassDetail(ClassDetail classdetail) {
		
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		
		session.save(classdetail);
		
		
		transaction.commit();
		session.close();
		return classdetail;
	}

	@Override
	public List<ClassDetail> getClassDetails() {

		Transaction transaction = session.beginTransaction();
		List<ClassDetail> classdetaillist = session.createQuery("from com.learneracademy.model.ClassDetail").list();
		return classdetaillist;
	}

	@Override
	public ClassDetail updateClassDetail(ClassDetail classdetail) {

		Transaction transaction = session.beginTransaction();
		
//		List<TeacherDetail> teachers = classdetail.getTeacherdetail();
//		for (TeacherDetail teacherDetail : teachers) {
//			
//			TeacherDetail t1 = (TeacherDetail) session.get(TeacherDetail.class,teacherDetail.getTeacherID());
//			t1.setClassteacherdetail(classdetail);
//			session.update(t1);
//		}
		transaction.commit();
		session.close();
		return classdetail;
	}

	@Override
	public ClassDetail getClassDetailById(int classId) {

		Transaction transaction = session.beginTransaction();
		ClassDetail classdetail = (ClassDetail) session.get(ClassDetail.class, classId);
		transaction.commit();
		session.close();
		return classdetail;
	}

	@Override
	public void removeClassDetail(int classId) {

		Transaction transaction = session.beginTransaction();
		ClassDetail classdetail = new ClassDetail();
		classdetail.setClassId(classId);
		session.delete(classdetail);
		transaction.commit();
		session.close();

	}

}
