package com.learneracademy.DataAccessObjectImplementation;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;


import com.learneracademy.DataAccessObject.StudentDetailDAO;
import com.learneracademy.model.ClassDetail;
import com.learneracademy.model.StudentDetail;

public class StudentDetailDAOImpl implements StudentDetailDAO {

	Configuration configuration = new Configuration().configure();

	StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
			.applySettings(configuration.getProperties());

	SessionFactory factory = configuration.buildSessionFactory(builder.build());

	Session session = factory.openSession();

	@Override
	public StudentDetail createStudentDetail(StudentDetail studentdetail) {

		Transaction transaction = session.beginTransaction();
		session.save(studentdetail);
		transaction.commit();
		session.close();
		return studentdetail;
	}

	@Override
	public List<StudentDetail> getStudentDetails() {
		Transaction transaction = session.beginTransaction();
		List<StudentDetail> studentdetaillist = session.createQuery("from com.learneracademy.model.StudentDetail").list();
		return studentdetaillist;
	}

	@Override
	public StudentDetail updateStudentDetail(StudentDetail studentdetail) {
		Transaction transaction=session.beginTransaction();
		session.update(studentdetail);
		transaction.commit();
		session.close();
		return studentdetail;
	}

	@Override
	public StudentDetail getStudentDetailById(int studentRoll) {
		Transaction transaction=session.beginTransaction();
		StudentDetail studentdetail=(StudentDetail) session.get(StudentDetail.class,studentRoll);
		transaction.commit();
		session.close();
		return studentdetail;
	}

	@Override
	public void removeStudentDetail(int studentRoll) {
		Transaction transaction=session.beginTransaction();
		StudentDetail studentdetail = new StudentDetail();
		studentdetail.setStudentRoll(studentRoll);
		session.delete(studentdetail);
		transaction.commit();
		session.close();
	}

}
