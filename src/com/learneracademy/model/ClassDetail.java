package com.learneracademy.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.learneracademy.model.SubjectDetail;

@Entity
@Table(name = "Class")
public class ClassDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CLASS_ID")
	private int classId;

	@Column(name = "CLASS_NAME", unique = true)
	private String className;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "teacherdetail")
	private Set<TeacherDetail> teacherdetail = new HashSet<TeacherDetail>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "classstuddetail")
	private Set<StudentDetail> studentdetail;

	public ClassDetail() {
		// TODO Auto-generated constructor stub
	}

	public int getClassId() {
		return classId;
	}

	public void setClassId(int classId) {
		this.classId = classId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Set<StudentDetail> getStudentdetail() {
		return studentdetail;
	}

	public void setStudentdetail(Set<StudentDetail> studentdetail) {
		this.studentdetail = studentdetail;
	}

	public Set<TeacherDetail> getTeacherdetail() {
		return teacherdetail;
	}

	public void setTeacherdetail(Set<TeacherDetail> teacherdetail) {
		this.teacherdetail = teacherdetail;
	}

	public ClassDetail(String className) {
		super();
		this.className = className;
	}

}