package com.learneracademy.model;


import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Student")
public class StudentDetail  {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int studentRoll;

	@Column(name = "STUDENT_NAME")
	private String studentName;
	@Column(name = "STUDENT_GENDER")
	private String studentGender;
	@Column(name = "STUDENT_AGE")

	private int studentAge;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "CLASS_ID")
	private ClassDetail classstuddetail;

	public StudentDetail() {
		// TODO Auto-generated constructor stub
	}

	public int getStudentRoll() {
		return studentRoll;
	}

	public void setStudentRoll(int studentRoll) {
		this.studentRoll = studentRoll;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentGender() {
		return studentGender;
	}

	public void setStudentGender(String studentGender) {
		this.studentGender = studentGender;
	}

	public int getStudentAge() {
		return studentAge;
	}

	public void setStudentAge(int studentAge) {
		this.studentAge = studentAge;
	}
	
	

	public ClassDetail getClassstuddetail() {
		return classstuddetail;
	}

	public void setClassstuddetail(ClassDetail classstuddetail) {
		this.classstuddetail = classstuddetail;
	}

	public StudentDetail(String studentName, String studentGender, int studentAge) {
		super();
		this.studentName = studentName;
		this.studentGender = studentGender;
		this.studentAge = studentAge;
	}


	


}
