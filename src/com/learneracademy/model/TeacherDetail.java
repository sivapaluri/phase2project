package com.learneracademy.model;


import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Teacher")
public class TeacherDetail implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "TEACHER_NUMBER")
	private int teacherID;

	@Column(name = "TEACHER_NAME")
	private String teacherName;

	@Column(name = "TEACHER_CONTACT")
	private long teacherContact;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "CLASS_ID")
	private ClassDetail teacherdetail;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "SUB_SERIAL_NUMBER")
	private SubjectDetail teachersubdetail;


	public TeacherDetail() {
		// TODO Auto-generated constructor stub
	}

	public int getTeacherID() {
		return teacherID;
	}

	public void setTeacherID(int teacherID) {
		this.teacherID = teacherID;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public long getTeacherContact() {
		return teacherContact;
	}

	public void setTeacherContact(long teacherContact) {
		this.teacherContact = teacherContact;
	}

	

	public ClassDetail getTeacherdetail() {
		return teacherdetail;
	}

	public void setTeacherdetail(ClassDetail teacherdetail) {
		this.teacherdetail = teacherdetail;
	}
	
	

	public SubjectDetail getTeachersubdetail() {
		return teachersubdetail;
	}

	public void setTeachersubdetail(SubjectDetail teachersubdetail) {
		this.teachersubdetail = teachersubdetail;
	}

	public TeacherDetail(String teacherName, long teacherContact) {
		super();
		this.teacherName = teacherName;
		this.teacherContact = teacherContact;
	}



	


	



}
