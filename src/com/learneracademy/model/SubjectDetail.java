package com.learneracademy.model;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Subject")

public class SubjectDetail implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "SUB_SERIAL_NUMBER")
	private int sNo;

	@Column(name = "SUBJECT_CODE")
	private String subjectCode;
	@Column(name = "SUBJECT_NAME")
	private String subjectName;
	
	
	@OneToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER,mappedBy = "teachersubdetail")
	private Set<TeacherDetail> teacherdetail = new HashSet<TeacherDetail>();

	

	public SubjectDetail() {
		// TODO Auto-generated constructor stub
	}

	public String getSubjectCode() {
		return subjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public int getsNo() {
		return sNo;
	}

	public void setsNo(int sNo) {
		this.sNo = sNo;
	}
	
	

	public Set<TeacherDetail> getTeacherdetail() {
		return teacherdetail;
	}

	public void setTeacherdetail(Set<TeacherDetail> teacherdetail) {
		this.teacherdetail = teacherdetail;
	}

	public SubjectDetail(String subjectCode, String subjectName) {
		super();
		this.subjectCode = subjectCode;
		this.subjectName = subjectName;
	}

	
	





	

	


}

	