package com.learneracademy.serviceImplementation;

import java.util.List;

import com.learneracademy.DataAccessObject.SubjectDetailDAO;
import com.learneracademy.DataAccessObjectImplementation.SubjectDetailDAOImpl;
import com.learneracademy.Service.SubjectDetailService;
import com.learneracademy.model.SubjectDetail;

public class SubjectDetailServiceImpl implements SubjectDetailService {
	
	SubjectDetailDAO subjectdao = new SubjectDetailDAOImpl();

	@Override
	public SubjectDetail createSubjectDetail(SubjectDetail subjectdetail) {
		// TODO Auto-generated method stub
		return subjectdao.createSubjectDetail(subjectdetail);
	}

	@Override
	public List<SubjectDetail> getSubjectDetails() {
		// TODO Auto-generated method stub
		return subjectdao.getSubjectDetails();
	}

	@Override
	public SubjectDetail updateSubjectDetail(SubjectDetail subjectdetail) {
		// TODO Auto-generated method stub
		return subjectdao.updateSubjectDetail(subjectdetail);
	}

	@Override
	public SubjectDetail getSubjectDetailById(int id) {
		// TODO Auto-generated method stub
		return subjectdao.getSubjectDetailById(id);
	}

	@Override
	public void removeSubjectDetail(int id) {
		// TODO Auto-generated method stub
		
	}

}
