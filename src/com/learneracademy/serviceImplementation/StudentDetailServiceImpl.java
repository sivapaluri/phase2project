package com.learneracademy.serviceImplementation;

import java.util.List;

import com.learneracademy.DataAccessObject.StudentDetailDAO;
import com.learneracademy.DataAccessObjectImplementation.StudentDetailDAOImpl;
import com.learneracademy.Service.StudentDetailService;
import com.learneracademy.model.StudentDetail;

public class StudentDetailServiceImpl implements StudentDetailService {
	
	private StudentDetailDAO studentdetaildao = new StudentDetailDAOImpl();

	@Override
	public StudentDetail createStudentdetail(StudentDetail studentdetail) {
		
		return studentdetaildao.createStudentDetail(studentdetail);
	}

	@Override
	public List<StudentDetail> getStudentDetails() {
		// TODO Auto-generated method stub
		return studentdetaildao.getStudentDetails();
	}

	@Override
	public StudentDetail updateStudentDetail(StudentDetail studentdetail) {
		// TODO Auto-generated method stub
		return studentdetaildao.updateStudentDetail(studentdetail);
	}

	@Override
	public StudentDetail getStudentDetailById(int studentRoll) {
		// TODO Auto-generated method stub
		return studentdetaildao.getStudentDetailById(studentRoll);
	}

	@Override
	public void removeStudentDetail(int studentRoll) {
		// TODO Auto-generated method stub
		
	}

	

}
