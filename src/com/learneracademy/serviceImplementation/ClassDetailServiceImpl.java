package com.learneracademy.serviceImplementation;

import java.util.List;

import com.learneracademy.DataAccessObject.ClassDetailDAO;
import com.learneracademy.DataAccessObjectImplementation.ClassDetailDAOImpl;
import com.learneracademy.Service.ClassDetailService;
import com.learneracademy.model.ClassDetail;

public class ClassDetailServiceImpl implements ClassDetailService {

	private ClassDetailDAO classdetaildao = new ClassDetailDAOImpl();
	
	
	@Override
	public ClassDetail createClassDetail(ClassDetail classdetail) {
		
		return classdetaildao.createClassDetail(classdetail);
	}


	@Override
	public List<ClassDetail> getClassDetails() {

		return classdetaildao.getClassDetails();
	}


	@Override
	public ClassDetail updateClassDetail(ClassDetail classdetail) {
		// TODO Auto-generated method stub
		return classdetaildao.updateClassDetail(classdetail);
	}


	@Override
	public ClassDetail getClassDetailById(int classId) {
		// TODO Auto-generated method stub
		return classdetaildao.getClassDetailById(classId);
	}


	@Override
	public void removeClassDetail(int classId) {
		// TODO Auto-generated method stub
		
	}

}
