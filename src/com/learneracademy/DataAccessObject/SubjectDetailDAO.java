package com.learneracademy.DataAccessObject;

import java.util.List;

import com.learneracademy.model.SubjectDetail;

public interface SubjectDetailDAO {

	public SubjectDetail createSubjectDetail(SubjectDetail subjectdetail);

	public List<SubjectDetail> getSubjectDetails();

	public SubjectDetail updateSubjectDetail(SubjectDetail subjectdetail);

	public SubjectDetail getSubjectDetailById(int id);

	public void removeSubjectDetail(int id);

}
